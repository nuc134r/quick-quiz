'use strict';

const port = 8045;

const path = require('path');
const express = require('express');
const serveStatic = require('serve-static');

const generator = require('./generator');
const questions = require('./questions');

let app = express();

app.set('view engine', 'jade');
app.set('views');

const css_path      = path.join(__dirname, 'public/css'),
      scritps_path  = path.join(__dirname, 'public/scripts'),
      assets_path   = path.join(__dirname, 'public/assets');

app.use(express.static(css_path));
app.use(express.static(scritps_path));
app.use(express.static(assets_path));

Array.prototype.toString = function () { return this.join(', ') }

app.use('/results', (req, res) => {
    let results = {};
    req.url.substr(2).split('&').map(_ => _.split('=')).forEach(result => {
        let questionIndex = result[0]; 
        let answer = result[1]; 
        
        if (results[questionIndex]) {
            if (typeof results[questionIndex] == 'object') {
                results[questionIndex].push(answer);
            } else {
                let answers = [results[questionIndex], answer];
                results[questionIndex] = answers;
            }
        } else {
            results[result[0]] = result[1]
        }
    });

    let html = generator.generateResultsPage(questions.topic, results, questions.list);
    res.end(html);
});

app.use('/', (req, res) => {
    res.render('layout', { questions, shuffle });
    
    /*let html = generator.generateQuizPage(questions.topic, questions.list);
    res.end(html);*/
});

app.listen(port, () => {
    console.log('Server running on ' + port);
});

// http://stackoverflow.com/a/2450976/5346885
function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}