'use strict';

module.exports.generateQuizPage = (topic, questions) => {
    let html = `
        <h1>Тест "${topic}"</h1>
        <form action="/results" method="GET">`;

    for (let i = 0; i < questions.length; i++) {
        let question = questions[i];

        html += `
            <p><b>${i + 1}. ${question.title} </b></p>
            <div>`;

        let optionsHtml = [];
        let inputType = question.options.correct.length > 1 ? 'checkbox' : 'radio'
        let options = question.options.correct.concat(question.options.incorrect)
        for (let j = 0; j < options.length; j++) {
            let option = options[j];
            optionsHtml.push(`
                <input name="${i}" type="${inputType}" value="${j}"> <span>${option}</span> 
                <br>`);
        }

        let shuffledOptions = shuffle(optionsHtml);
        for (let j = 0; j < shuffledOptions.length; j++) {
            html += shuffledOptions[j];
        }

        html += `
            </div>`;
    }

    html += `
            <br>
            <br>
            <input type="submit" value="Отправить">
        </form>`;

    return generatePage('Опрос', html);
}

module.exports.generateResultsPage = (topic, results, questions) => {
    let html = `
        <h1>Результаты теста "${topic}"</h1>`;

    let corrects = 0;
    let incorrects = 0;

    for (let i = 0; i < questions.length; i++) {
        let question = questions[i];
        let options = question.options.correct.concat(question.options.incorrect);
        let correctOptions = question.options.correct.length;

        let givenAnswer = results[i];

        html += `
            <b> ${question.title} </b>
            <br>`;

        if (!givenAnswer) {
            html += '<span style="color: red;"> Ответ не дан </span>';
            incorrects++;
        } else {

            if (typeof givenAnswer == 'object') {
                if (givenAnswer.every(_ => _ < correctOptions)) {
                    html += `${givenAnswer.map(_ => options[_]).join(', ')} <br> <span style="color: green;"> <b>(Верно)</b> </span>`;
                    corrects++;
                } else {
                    html += `${givenAnswer.map(_ => options[_]).join(', ')} <br> <span style="color: red;"> <b>(Неверно)</b> </span>
                    <br>
                    Правильный ответ: ${question.options.correct.join(', ')}`;
                    incorrects++;
                }
            } else {
                if (givenAnswer == 0 && correctOptions == 1) {
                    html += `${options[givenAnswer]} <br> <span style="color: green;"> <b>(Верно)</b> </span>`;
                    corrects++;
                } else {
                    html += `
                    ${options[givenAnswer]} <br> <span style="color: red;"> <b>(Неверно)</b> </span>
                    <br>
                    Правильный ответ: ${question.options.correct}`;
                    incorrects++;
                }
            }
        }

        html += `
            <br>
            <br>`;
    }

    html += `
    Всего вопросов: ${questions.length}
    <br>
    Верных ответов: ${corrects} (${Math.floor(corrects * 100 / questions.length)}%)
    <br>
    <br>
    <form action="/">
        <input type="submit" value="Пройти ещё раз" />
    </form>`;

    return generatePage('Результаты', html);
}

// http://stackoverflow.com/a/2450976/5346885
function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function generatePage(title, body) {
    return `
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>${title}</title>
</head>

<body>
    ${body}
</body>

</html>`
}